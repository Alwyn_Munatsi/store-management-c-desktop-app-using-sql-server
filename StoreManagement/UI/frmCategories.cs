﻿using StoreManagement.BLL;
using StoreManagement.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.UI
{
    public partial class frmCategories : Form
    {
        public frmCategories()
        {
            InitializeComponent();
        }

        categoriesBLL c = new categoriesBLL();
        categoriesDAL dal = new categoriesDAL();
        userDAL udal = new userDAL();

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            //Getting data from UI
            c.title = txtTitle.Text;
            c.description = txtDescription.Text;
            c.added_date = DateTime.Now;

            //Getting ID in added by field
            string loggedUser = frmLogin.loggedIn;
            userBLL usr = udal.GetIDFromUsername(loggedUser);
            //Passing the id of logged in user in added_by field
            c.added_by = usr.Id;

            //Inserting data into database
            bool success = dal.Insert(c);

            //If the data is successfully inserted then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully Inserted
                MessageBox.Show("Category successfully created.");
                clear();

                //Refresh data in data grid view
                DataTable dt = dal.Select();
                dgvCategories.DataSource = dt;
            }
            else
            {
                //Failed to insert data
                MessageBox.Show("Failed to add new category.");
            }

            }

            private void clear()
            {
                txtCategoryID.Text = "";
                txtTitle.Text = "";
                txtDescription.Text = "";
            }

        private void frmCategories_Load(object sender, EventArgs e)
        {
            //Here we write the code to display all the Data in the grid view
            DataTable dt = dal.Select();
            dgvCategories.DataSource = dt;
        }

        private void dgvCategories_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Get the index of particular row
            int rowIndex = e.RowIndex;
            txtCategoryID.Text = dgvCategories.Rows[rowIndex].Cells[0].Value.ToString();
            txtTitle.Text = dgvCategories.Rows[rowIndex].Cells[1].Value.ToString();
            txtDescription.Text = dgvCategories.Rows[rowIndex].Cells[2].Value.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Getting the values from Categories form
            c.Id = int.Parse(txtCategoryID.Text);
            c.title = txtTitle.Text;
            c.description = txtDescription.Text;
            c.added_date = DateTime.Now;

            //Getting username of logged in user
            string loggedUser = frmLogin.loggedIn;

            userBLL usr = udal.GetIDFromUsername(loggedUser);
            c.added_by = usr.Id;

            //Update data into database
            bool success = dal.Update(c);

            //If the data is successfully updated then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully updated
                MessageBox.Show("Category successfully updated.");
                clear();
            }
            else
            {
                //Failed to update data
                MessageBox.Show("Failed to Update Category.");
            }
            //Refreshing data grid view
            DataTable dt = dal.Select();
            dgvCategories.DataSource = dt;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Getting the values from Category 
            c.Id = int.Parse(txtCategoryID.Text);

            //Delete data from database
            bool success = dal.Delete(c);

            //If the data is successfully deleted then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully deleted
                MessageBox.Show("Category successfully deleted.");
                clear();
            }
            else
            {
                //Failed to delete data
                MessageBox.Show("Failed to delete Category.");
            }
            //Refreshing data grid view
            DataTable dt = dal.Select();
            dgvCategories.DataSource = dt;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //Get keywords from Textbox
            string keywords = txtSearch.Text;

            //Check if the keywords has value or not
            if (keywords != null)
            {
                //Show user based on keyword
                DataTable dt = dal.Search(keywords);
                dgvCategories.DataSource = dt;
            }
            else
            {
                //Show all users from the database
                DataTable dt = dal.Select();
                dgvCategories.DataSource = dt;
            }
        }
    }
}
 
