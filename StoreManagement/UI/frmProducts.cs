﻿using StoreManagement.BLL;
using StoreManagement.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.UI
{
    public partial class frmProducts : Form
    {
        public frmProducts()
        {
            InitializeComponent();
        }

        productsBLL p = new productsBLL();
        productsDAL dal = new productsDAL();
        userDAL udal = new userDAL();

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        categoriesDAL cdal = new categoriesDAL();
        private void frmProducts_Load(object sender, EventArgs e)
        {
            //Creating DataTable to hold categories from database
            DataTable categoriesDT = cdal.Select();
            //Specify datasource for category combobox
            cmbCategory.DataSource = categoriesDT;
            //Specify display member and value member for combobox
            cmbCategory.DisplayMember = "title";
            cmbCategory.ValueMember = "title";

            //Here we write the code to display all the Data in the grid view
            DataTable dt = dal.Select();
            dgvProducts.DataSource = dt;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Getting all values from products form
            p.name = txtName.Text;
            p.category = cmbCategory.Text;
            p.description = txtDescription.Text;
            p.rate = decimal.Parse(txtRate.Text);
            p.qty = 0;
            p.added_date = DateTime.Now;

            //Getting username of loggedin user
            string loggedUser = frmLogin.loggedIn;
            userBLL usr = udal.GetIDFromUsername(loggedUser);
            //Passing the id of logged in user in added_by field
            p.added_by = usr.Id;

            //Inserting data into database
            bool success = dal.Insert(p);

            //If the data is successfully inserted then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully Inserted
                MessageBox.Show("Product successfully created.");
                clear();

                //Refresh data in data grid view
                DataTable dt = dal.Select();
                dgvProducts.DataSource = dt;
            }
            else
            {
                //Failed to insert data
                MessageBox.Show("Failed to add new product.");
            }

        }

        private void clear()
        {
            txtProductID.Text = "";
            txtName.Text = "";
            cmbCategory.Text = "";
            txtDescription.Text = "";
            txtRate.Text = "";
        }

        private void dgvProducts_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Get the index of particular row
            int rowIndex = e.RowIndex;
            txtProductID.Text = dgvProducts.Rows[rowIndex].Cells[0].Value.ToString();
            txtName.Text = dgvProducts.Rows[rowIndex].Cells[1].Value.ToString();
            cmbCategory.Text = dgvProducts.Rows[rowIndex].Cells[2].Value.ToString();
            txtDescription.Text = dgvProducts.Rows[rowIndex].Cells[3].Value.ToString();
            txtRate.Text = dgvProducts.Rows[rowIndex].Cells[4].Value.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Getting the values from Products form
            p.Id = int.Parse(txtProductID.Text);
            p.name = txtName.Text;
            p.category = cmbCategory.Text;
            p.description = txtDescription.Text;
            p.rate = decimal.Parse(txtRate.Text);
            p.added_date = DateTime.Now;

            //Getting username of logged in user
            string loggedUser = frmLogin.loggedIn;

            userBLL usr = udal.GetIDFromUsername(loggedUser);
            p.added_by = usr.Id;

            //Update data into database
            bool success = dal.Update(p);

            //If the data is successfully updated then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully updated
                MessageBox.Show("Product successfully updated.");
                clear();
            }
            else
            {
                //Failed to update data
                MessageBox.Show("Failed to Update Product.");
            }
            //Refreshing data grid view
            DataTable dt = dal.Select();
            dgvProducts.DataSource = dt;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Getting the values from Product 
            p.Id = int.Parse(txtProductID.Text);

            //Delete data from database
            bool success = dal.Delete(p);

            //If the data is successfully deleted then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully deleted
                MessageBox.Show("Product successfully deleted.");
                clear();
            }
            else
            {
                //Failed to delete data
                MessageBox.Show("Failed to delete Product.");
            }
            //Refreshing data grid view
            DataTable dt = dal.Select();
            dgvProducts.DataSource = dt;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //Get keywords from Textbox
            string keywords = txtSearch.Text;

            //Check if the keywords has value or not
            if (keywords != null)
            {
                //Show user based on keyword
                DataTable dt = dal.Search(keywords);
                dgvProducts.DataSource = dt;
            }
            else
            {
                //Show all users from the database
                DataTable dt = dal.Select();
                dgvProducts.DataSource = dt;
            }
        }
    }
}
