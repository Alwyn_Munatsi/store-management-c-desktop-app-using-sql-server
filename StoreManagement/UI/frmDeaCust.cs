﻿using StoreManagement.BLL;
using StoreManagement.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.UI
{
    public partial class frmDeaCust : Form
    {
        public frmDeaCust()
        {
            InitializeComponent();
        }

        deacustBLL d = new deacustBLL();
        deacustDAL dal = new deacustDAL();
        userDAL udal = new userDAL();

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Getting all values from products form
            d.type = cmbType.Text;
            d.name = txtName.Text;
            d.email = txtEmail.Text;
            d.contact = txtContact.Text;
            d.address = txtAddress.Text;
            d.added_date = DateTime.Now;

            //Getting username of loggedin user
            string loggedUser = frmLogin.loggedIn;
            userBLL usr = udal.GetIDFromUsername(loggedUser);
            //Passing the id of logged in user in added_by field
            d.added_by = usr.Id;

            //Inserting data into database
            bool success = dal.Insert(d);

            //If the data is successfully inserted then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully Inserted
                MessageBox.Show("Dealer or Customer successfully created.");
                clear();

                //Refresh data in data grid view
                DataTable dt = dal.Select();
                dgvDeaCust.DataSource = dt;
            }
            else
            {
                //Failed to insert data
                MessageBox.Show("Failed to add Deaker or Customer.");
            }
        }

        private void clear()
        {
            txtDeaCustID.Text = "";
            cmbType.Text = "";
            txtName.Text = "";
            txtEmail.Text = "";
            txtContact.Text = "";
            txtAddress.Text = "";
        }

        private void dgvDeaCust_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Get the index of particular row
            int rowIndex = e.RowIndex;
            txtDeaCustID.Text = dgvDeaCust.Rows[rowIndex].Cells[0].Value.ToString();
            cmbType.Text = dgvDeaCust.Rows[rowIndex].Cells[1].Value.ToString();
            txtName.Text = dgvDeaCust.Rows[rowIndex].Cells[2].Value.ToString();
            txtEmail.Text = dgvDeaCust.Rows[rowIndex].Cells[3].Value.ToString();
            txtContact.Text = dgvDeaCust.Rows[rowIndex].Cells[4].Value.ToString();
            txtAddress.Text = dgvDeaCust.Rows[rowIndex].Cells[5].Value.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Getting the values from Products form
            d.Id = int.Parse(txtDeaCustID.Text);
            d.type = cmbType.Text;
            d.name = txtName.Text;
            d.email = txtEmail.Text;
            d.contact = txtContact.Text;
            d.address = txtAddress.Text;
            d.added_date = DateTime.Now;

            //Getting username of logged in user
            string loggedUser = frmLogin.loggedIn;

            userBLL usr = udal.GetIDFromUsername(loggedUser);
            d.added_by = usr.Id;

            //Update data into database
            bool success = dal.Update(d);

            //If the data is successfully updated then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully updated
                MessageBox.Show("Dealer or Customer successfully updated.");
                clear();
            }
            else
            {
                //Failed to update data
                MessageBox.Show("Failed to Update Dealer or Customer.");
            }
            //Refreshing data grid view
            DataTable dt = dal.Select();
            dgvDeaCust.DataSource = dt;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Getting the values from Product 
            d.Id = int.Parse(txtDeaCustID.Text);

            //Delete data from database
            bool success = dal.Delete(d);

            //If the data is successfully deleted then the value will be true else it will be false
            if (success == true)
            {
                //Data Successfully deleted
                MessageBox.Show("Dealer or Customer successfully deleted.");
                clear();
            }
            else
            {
                //Failed to delete data
                MessageBox.Show("Failed to delete Dealer or Customer.");
            }
            //Refreshing data grid view
            DataTable dt = dal.Select();
            dgvDeaCust.DataSource = dt;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //Get keywords from Textbox
            string keywords = txtSearch.Text;

            //Check if the keywords has value or not
            if (keywords != null)
            {
                //Show user based on keyword
                DataTable dt = dal.Search(keywords);
                dgvDeaCust.DataSource = dt;
            }
            else
            {
                //Show all users from the database
                DataTable dt = dal.Select();
                dgvDeaCust.DataSource = dt;
            }
        }
    }
}
