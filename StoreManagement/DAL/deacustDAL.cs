﻿using StoreManagement.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.DAL
{
    class deacustDAL
    {
        static string myconnstrng = ConfigurationManager.ConnectionStrings["connstrng"].ConnectionString;

        #region Select Data from Database
        public DataTable Select()
        {
            //Static Method to connect to database
            SqlConnection conn = new SqlConnection(myconnstrng);
            //To hold the data from the database
            DataTable dt = new DataTable();

            try
            {
                //Sql query to get data from the database
                string sql = "SELECT * FROM tbl_dea_cust";
                //For executing command
                SqlCommand cmd = new SqlCommand(sql, conn);
                //Getting data from database
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                //Database connection open
                conn.Open();
                //Fill data in our datatable
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                //Throw Message if any error occurs
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Closing connection
                conn.Close();
            }
            //Return the value in the datatable
            return dt;
        }
        #endregion

        #region Insert Data in Database
        public bool Insert(deacustBLL d)
        {
            bool isSuccess = false;
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                string sql = "INSERT INTO tbl_dea_cust (type, name, email, contact, address, added_date, added_by) VALUES (@type, @name, @email, @contact, @address, @added_date, @added_by)";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@type", d.type);
                cmd.Parameters.AddWithValue("@name", d.name);
                cmd.Parameters.AddWithValue("@email", d.email);
                cmd.Parameters.AddWithValue("@contact", d.contact);
                cmd.Parameters.AddWithValue("@address", d.address);
                cmd.Parameters.AddWithValue("@added_date", d.added_date);
                cmd.Parameters.AddWithValue("@added_by", d.added_by);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();

                //if the query is executed successfully then the value to rows will be greater than 0 else it will be less than 0
                if (rows > 0)
                {
                    //Query Successfull
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return isSuccess;
        }
        #endregion

        #region Update data in Database
        public bool Update(deacustBLL d)
        {
            bool isSuccess = false;
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                string sql = "UPDATE tbl_dea_cust SET type=@type, name=@name, email=@email, contact=@contact, address=@address, added_date=@added_date, added_by=@added_by WHERE Id=@Id";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@type", d.type);
                cmd.Parameters.AddWithValue("@name", d.name);
                cmd.Parameters.AddWithValue("@email", d.email);
                cmd.Parameters.AddWithValue("@contact", d.contact);
                cmd.Parameters.AddWithValue("@address", d.address);
                cmd.Parameters.AddWithValue("@added_date", d.added_date);
                cmd.Parameters.AddWithValue("@added_by", d.added_by);
                cmd.Parameters.AddWithValue("@Id", d.Id);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();

                //if the query is executed successfully then the value to rows will be greater than 0 else it will be less than 0
                if (rows > 0)
                {
                    //Query Successfull
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return isSuccess;
        }
        #endregion

        #region Delete Data from Database
        public bool Delete(deacustBLL d)
        {
            bool isSuccess = false;
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                string sql = "DELETE FROM tbl_dea_cust WHERE Id=@Id";
                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Id", d.Id);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();

                //if the query is executed successfully then the value to rows will be greater than 0 else it will be less than 0
                if (rows > 0)
                {
                    //Query Successfull
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return isSuccess;
        }
        #endregion

        #region Search User on database using keywords
        public DataTable Search(string keywords)
        {
            //Static Method to connect to database
            SqlConnection conn = new SqlConnection(myconnstrng);
            //To hold the data from the database
            DataTable dt = new DataTable();

            try
            {
                //Sql query to get data from the database
                string sql = "SELECT * FROM tbl_dea_cust WHERE Id LIKE '%" + keywords + "%' OR type LIKE '%" + keywords + "%' OR name LIKE '%" + keywords + "%'";
                //For executing command
                SqlCommand cmd = new SqlCommand(sql, conn);
                //Getting data from database
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                //Database connection open
                conn.Open();
                //Fill data in our datatable
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                //Throw Message if any error occurs
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Closing connection
                conn.Close();
            }
            //Return the value in the datatable
            return dt;
        }
        #endregion

        #region Method to search Dealer or Customer for transaction module
        public deacustBLL SearchDealerCustomerForTransaction(string keyword)
        {
            //Create an object for deacustBLL clas
            deacustBLL dc = new deacustBLL();
            //Create a database connection
            SqlConnection conn = new SqlConnection(myconnstrng);

            //Create a DataTable to hold the value temporarily
            DataTable dt = new DataTable();

            try
            {
                //Write an Sql query to search dealer or customer based on keywords
                string sql = "SELECT name, email, contact, address FROM tbl_dea_cust WHERE Id LIKE '%" + keyword + "%' OR name LIKE '%" + keyword + "%'";

                //Create a SqlDataAdapter to execute the query
                SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                //Open the database connection
                conn.Open();
                //Transfer the data from SqlDataAdapter to DataTable
                adapter.Fill(dt);

                //If we have values on dt we need to save it in deacustBLL
                if(dt.Rows.Count>0)
                {
                    dc.name = dt.Rows[0]["name"].ToString();
                    dc.email = dt.Rows[0]["email"].ToString();
                    dc.contact = dt.Rows[0]["contact"].ToString();
                    dc.address = dt.Rows[0]["address"].ToString();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dc;
        }
        #endregion
    }
}
