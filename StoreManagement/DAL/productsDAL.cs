﻿using StoreManagement.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.DAL
{
    class productsDAL
    {
        static string myconnstrng = ConfigurationManager.ConnectionStrings["connstrng"].ConnectionString;

        #region Select Data from Database
        public DataTable Select()
        {
            //Static Method to connect to database
            SqlConnection conn = new SqlConnection(myconnstrng);
            //To hold the data from the database
            DataTable dt = new DataTable();

            try
            {
                //Sql query to get data from the database
                string sql = "SELECT * FROM tbl_products";
                //For executing command
                SqlCommand cmd = new SqlCommand(sql, conn);
                //Getting data from database
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                //Database connection open
                conn.Open();
                //Fill data in our datatable
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                //Throw Message if any error occurs
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Closing connection
                conn.Close();
            }
            //Return the value in the datatable
            return dt;
        }
        #endregion

        #region Insert Data in Database
        public bool Insert(productsBLL p)
        {
            bool isSuccess = false;
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                string sql = "INSERT INTO tbl_products (name, category, description, rate, qty, added_date, added_by) VALUES (@name,@category,@description,@rate,@qty,@added_date,@added_by)";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@name", p.name);
                cmd.Parameters.AddWithValue("@category", p.category);
                cmd.Parameters.AddWithValue("@description", p.description);
                cmd.Parameters.AddWithValue("@rate", p.rate);
                cmd.Parameters.AddWithValue("@qty", p.qty);
                cmd.Parameters.AddWithValue("@added_date", p.added_date);
                cmd.Parameters.AddWithValue("@added_by", p.added_by);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();

                //if the query is executed successfully then the value to rows will be greater than 0 else it will be less than 0
                if (rows > 0)
                {
                    //Query Successfull
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return isSuccess;
        }
        #endregion

        #region Update data in Database
        public bool Update(productsBLL p)
        {
            bool isSuccess = false;
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                string sql = "UPDATE tbl_products SET name=@name, category=@category, description=@description, rate=@rate, qty=@qty,added_date=@added_date, added_by=@added_by WHERE Id=@Id";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@name", p.name);
                cmd.Parameters.AddWithValue("@category", p.category);
                cmd.Parameters.AddWithValue("@description", p.description);
                cmd.Parameters.AddWithValue("@rate", p.rate);
                cmd.Parameters.AddWithValue("@qty", p.qty);
                cmd.Parameters.AddWithValue("@added_date", p.added_date);
                cmd.Parameters.AddWithValue("@added_by", p.added_by);
                cmd.Parameters.AddWithValue("@Id", p.Id);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();

                //if the query is executed successfully then the value to rows will be greater than 0 else it will be less than 0
                if (rows > 0)
                {
                    //Query Successfull
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return isSuccess;
        }
        #endregion

        #region Delete Data from Database
        public bool Delete(productsBLL p)
        {
            bool isSuccess = false;
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                string sql = "DELETE FROM tbl_products WHERE Id=@Id";
                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Id", p.Id);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();

                //if the query is executed successfully then the value to rows will be greater than 0 else it will be less than 0
                if (rows > 0)
                {
                    //Query Successfull
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return isSuccess;
        }
        #endregion

        #region Search User on database using keywords
        public DataTable Search(string keywords)
        {
            //Static Method to connect to database
            SqlConnection conn = new SqlConnection(myconnstrng);
            //To hold the data from the database
            DataTable dt = new DataTable();

            try
            {
                //Sql query to get data from the database
                string sql = "SELECT * FROM tbl_products WHERE Id LIKE '%" + keywords + "%' OR name LIKE '%" + keywords + "%' OR category LIKE '%" + keywords + "%' OR description LIKE '%" + keywords + "%'";
                //For executing command
                SqlCommand cmd = new SqlCommand(sql, conn);
                //Getting data from database
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                //Database connection open
                conn.Open();
                //Fill data in our datatable
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                //Throw Message if any error occurs
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Closing connection
                conn.Close();
            }
            //Return the value in the datatable
            return dt;
        }
        #endregion

        #region Method to Search product in Transaction Module
        public productsBLL GetProductsForTransaction(string keyword)
        {
            //Create an object of productsBLL and return it
            productsBLL p = new productsBLL();
            //Sql Connection
            SqlConnection conn = new SqlConnection(myconnstrng);
            //DataTable to store data temporarily
            DataTable dt = new DataTable();

            try
            {
                //Write the query to get the details
                string sql = "SELECT name,rate,qty FROM tbl_products WHERE Id LIKE '%" + keyword + "%' OR name LIKE '%" + keyword + "%'";
                //Create SqlDataAdapter to Execute the query
                SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                //Open database connection
                conn.Open();
                //pass the value for adapter to dt
                adapter.Fill(dt);

                //If we have have any values in dt then pass the set of values to productsBLL
                if(dt.Rows.Count>0)
                {
                    p.name = dt.Rows[0]["name"].ToString();
                    p.rate = decimal.Parse(dt.Rows[0]["rate"].ToString());
                    p.qty = decimal.Parse(dt.Rows[0]["qty"].ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Close Connection
                conn.Close();
            }
            return p;
        }
        #endregion
    }
}
