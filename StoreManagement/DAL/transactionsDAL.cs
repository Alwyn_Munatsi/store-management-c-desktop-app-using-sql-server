﻿using StoreManagement.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.DAL
{
    class transactionsDAL
    {
        static string myconnstrng = ConfigurationManager.ConnectionStrings["connstrng"].ConnectionString;

        #region Insert Transaction Method
        public bool Insert_Transaction(transactionsBLL t, out int transactionID)
        {
            //Create a boolean value and set its default value to false
            bool isSuccess = false;
            //Set the out transaction  value to -1
            transactionID = -1;
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                //Sql query to insert transactions 
                string sql = "INSERT INTO tbl_transactions(type,dea_cust_Id,grandTotal,transaction_date,tax,discount,added_by) VALUES(@type,@dea_cust_Id,@grandTotal,@transaction_date,@tax,@discount,@added_by)";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@type", t.type);
                cmd.Parameters.AddWithValue("@dea_cust_Id", t.dea_cust_Id);
                cmd.Parameters.AddWithValue("@grandTotal", t.grandTotal);
                cmd.Parameters.AddWithValue("@transaction_date", t.transaction_date);
                cmd.Parameters.AddWithValue("@tax", t.tax);
                cmd.Parameters.AddWithValue("discount", t.discount);
                cmd.Parameters.AddWithValue("@added_by", t.added_by);

                conn.Open();

                object o = cmd.ExecuteScalar();

                //if the query is executed successfully then the value  will not be null else will be null
                if (o != null)
                {
                    //Query Successfull
                    transactionID = int.Parse(o.ToString());
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Close Connection
                conn.Close();
            }
            return isSuccess;
        }

        #endregion
    }
}
