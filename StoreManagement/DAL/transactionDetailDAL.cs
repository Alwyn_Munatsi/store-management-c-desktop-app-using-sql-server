﻿using StoreManagement.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreManagement.DAL
{
    class transactionDetailDAL
    {
        static string myconnstrng = ConfigurationManager.ConnectionStrings["connstrng"].ConnectionString;

        #region Insert Transaction Method
        public bool Insert_Transaction(transactionDetailBLL dt)
        {
            //Create a boolean value and set its default value to false
            bool isSuccess = false;
            
            SqlConnection conn = new SqlConnection(myconnstrng);

            try
            {
                //Sql query to insert transactions 
                string sql = "INSERT INTO tbl_transaction_detail(product_Id,rate,qty,total,dea_cust_Id,added_date,added_by) VALUES(@product_Id,@rate,@qty,@total,@dea_cust_Id,@added_date,@added_by)";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@product_Id", dt.product_Id);
                cmd.Parameters.AddWithValue("@rate", dt.rate);
                cmd.Parameters.AddWithValue("@qty", dt.qty);
                cmd.Parameters.AddWithValue("@total", dt.total);
                cmd.Parameters.AddWithValue("@dea_cust_Id", dt.dea_cust_id);
                cmd.Parameters.AddWithValue("added_date", dt.added_date);
                cmd.Parameters.AddWithValue("@added_by", dt.added_by);

                conn.Open();

                int rows = cmd.ExecuteNonQuery();

                //if the query is executed successfully then the value  will not be null else will be null
                if (rows > 0)
                {
                    //Query Successfull
                    isSuccess = true;
                }
                else
                {
                    //Query failed
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Close Connection
                conn.Close();
            }
            return isSuccess;
        }

        #endregion
    }
}
